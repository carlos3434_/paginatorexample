<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Filters\ProductFilter;
use Illuminate\Database\Eloquent\Builder;

class Product extends Model
{
    public function scopeFilter(Builder $builder, $request)
    {
        return (new ProductFilter($request))->filter($builder);
    }
}
