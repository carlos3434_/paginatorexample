<?php
namespace App\Filters\Product;

class EstadoFilter
{
    public function filter($builder, $value)
    {
        return $builder->where('estado', $value);
    }
}

