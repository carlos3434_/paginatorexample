<?php

namespace App\Filters;

use App\Filters\AbstractFilter;

class ProductFilter extends AbstractFilter
{
    protected $filters = [
        'estado'        => Product\EstadoFilter::class,
    ];
}