<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Auth::loginUsingId(1);
        $contador = 1;
        do {
            Product::create([
                'precio'        => 12.2,
                'estado'                => true,
                'nombre'                => 'producto de lavacarros',
                //'created_by'                => 1
            ]);
            $contador++;
        } while ( $contador <= 20000);
    }
}
