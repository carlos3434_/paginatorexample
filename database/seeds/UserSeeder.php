<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'        => 'juan',
            'email'       => 'carlos3434@hotmail.com',
            'password'    => encrypt('12345678'),
            //'created_by'                => 1
        ]);
    }
}
